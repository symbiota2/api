# Symbiota2 API

This is a monorepo for the various Symbiota2 API packages and plugins. It is managed by
[lerna](https://github.com/lerna/lerna).

### Packages
- The [config package](./packages/config) ([@symbiota2/api-config](https://www.npmjs.com/package/@symbiota2/api-config)) 
is responsible for loading configuration values from environment variables into Symbiota2.
- The [database package](./packages/database) ([@symbiota2/api-database](https://www.npmjs.com/package/@symbiota2/api-database)) 
is responsible for access to the Symbiota2 MySQL/MariaDB database
- The [auth package](./packages/auth) ([@symbiota2/api-auth](https://www.npmjs.com/package/@symbiota2/api-auth)) 
is responsible for authenticating and authorizing users.
- The [core API server package](packages/api-server) ([@symbiota2/api-server](https://www.npmjs.com/package/@symbiota2/api-server)) 
is responsible for actually running the app, and depends upon all the above packages.
- The [UI package](packages/ui) ([@symbiota2/ui](https://www.npmjs.com/package/@symbiota2/ui)) 
houses the angular app for the Symbiota2 frontend

<img src="./docs/s2-api.png" alt="s2-api-components">

### Plugins
- The [occurrence plugin](./plugins/occurrence) 
([@symbiota2/api-plugin-occurrence](https://www.npmjs.com/package/@symbiota2/api-plugin-occurrence)) provides API routes 
for accessing occurrence data

### Development
1. Build and link the packages and plugins:
    ```console
    $ npm i
    $ npm run bootstrap
    ```
2. Start the database server
    ```console
    $ docker-compose -f docker-compose.dev.yml up -d
    ```
3. Run database migrations
    ```console
    $ npm run typeorm:migration:run 
    ```
   
4. Start the development server
    ```console
    $ npm run start:dev
    ```
5. The backend will be available at http://127.0.0.1:8080/docs/

### Creating a plugin
1. Clone the [sample plugin repo](https://gitlab.com/symbiota2/sample-api-plugin)
into [plugins/](./plugins)
   
2. Use existing plugins by adding them to your plugins's
   [peer dependencies](https://nodejs.org/en/blog/npm/peer-dependencies/)
   and [importing them](./plugins/occurrence/src/occurrence.service.ts#L2)
   
3. Optionally add a [build script](./package.json#L27) for your plugin

4. Build the plugin

5. Add the plugin to the 
   [api server's configuration](./packages/api-server/src/plugin/plugin.config.ts)
   
6. Start the server using the steps above, and any 
   [controllers](https://docs.nestjs.com/controllers) you've defined in your 
   plugin should now be available at http://127.0.0.1:8080/docs/
