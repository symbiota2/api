import { Controller, Get, Param, Query, Post, Body, HttpStatus, HttpCode, Delete, NotFoundException, Patch } from '@nestjs/common';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { CollectionService } from './collection.service';
import {
    CollectionInstitutionOutputDto,
    CollectionOutputDto
} from './dto/Collection.output.dto';
import { CollectionFindAllParams } from './dto/coll-find-all.input.dto';
import { InstitutionService } from '../institution/institution.service';
import {
    CollectionInputDto,
    UpdateCollectionInputDto
} from './dto/Collection.input.dto';

@ApiTags('Collections')
@Controller('collections')
export class CollectionController {
    constructor(
        private readonly institutionService: InstitutionService,
        private readonly collections: CollectionService) { }

    @Get()
    @ApiResponse({ status: HttpStatus.OK, type: CollectionOutputDto, isArray: true })
    async findAll(@Query() findAllParams: CollectionFindAllParams): Promise<CollectionOutputDto[]> {
        const collections = await this.collections.findAll(findAllParams);
        const collectionDtos = collections.map(async (c) => {
            const collection = new CollectionOutputDto(c);
            const institution = await this.institutionService.findByID(
                c.institutionID
            );
            collection.institution = {
                id: institution.id,
                name: institution.name
            };
            return collection;
        });
        return Promise.all(collectionDtos);
    }

    @Get(':id')
    @ApiResponse({ status: HttpStatus.OK, type: CollectionOutputDto })
    async findByID(@Param('id') id: number): Promise<CollectionOutputDto> {
        const collection = await this.collections.findByID(id);
        const dto = new CollectionOutputDto(collection);
        dto.institution = new CollectionInstitutionOutputDto(await collection.institution);
        return dto;
    }

    @Post()
    @HttpCode(HttpStatus.OK)
    @ApiResponse({ status: HttpStatus.OK, type: CollectionOutputDto })
    async create(@Body() data: CollectionInputDto): Promise<CollectionOutputDto> {
        const collection = await this.collections.create(data);
        return new CollectionOutputDto(collection);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    @ApiResponse({ status: HttpStatus.NO_CONTENT })
    async deleteByID(@Param('id') id: number): Promise<void> {
        const collection = await this.collections.deleteByID(id);
        if (!collection) {
            throw new NotFoundException();
        }
    }

    @Patch(':id')
    @ApiResponse({ status: HttpStatus.OK, type: CollectionOutputDto })
    async updateByID(@Param('id') id: number, @Body() data: UpdateCollectionInputDto): Promise<CollectionOutputDto> {
        const collection = await this.collections.updateByID(id, data);
        if (!collection) {
            throw new NotFoundException();
        }
        return new CollectionOutputDto(collection);
    }
}
