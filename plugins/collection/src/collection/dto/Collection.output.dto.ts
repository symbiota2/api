import { Exclude, Expose, Transform, Type } from 'class-transformer';
import { Collection, Institution } from '@symbiota2/api-database';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class CollectionInstitutionOutputDto {
    constructor(institution: Institution) {
        Object.assign(this, institution);
    }

    @ApiProperty()
    @Expose()
    id: number;

    @ApiProperty()
    @Expose()
    name: string;
}

@Exclude()
export class CollectionOutputDto {
    constructor(collection: Collection) {
        Object.assign(this, collection);
    }

    @ApiProperty()
    @Expose()
    id: number;

    @ApiProperty()
    @Expose()
    collectionCode: string;

    @ApiProperty()
    @Expose()
    collectionName: string;

    @ApiProperty()
    @Expose()
    @Type(() => CollectionInstitutionOutputDto)
    institution: CollectionInstitutionOutputDto;

    @ApiProperty()
    @Expose()
    fullDescription: string;

    @ApiProperty()
    @Expose()
    homePage: string;

    @ApiProperty()
    @Expose()
    individualUrl: string;

    @ApiProperty()
    @Expose()
    contact: string;

    @ApiProperty()
    @Expose()
    email: string;

    // Lat/Lng numbers are too long for JS
    @ApiProperty()
    @Expose()
    latitude: string;

    @ApiProperty()
    @Expose()
    longitude: string;

    @ApiProperty()
    @Expose()
    icon: string;

    @ApiProperty()
    @Expose()
    type: string;

    @ApiProperty()
    @Expose()
    managementType: string;

    @ApiProperty()
    @Expose()
    rightsHolder: string;

    @ApiProperty()
    @Expose()
    rights: string;

    @ApiProperty()
    @Expose()
    usageTerm: string;

    @ApiProperty()
    @Expose()
    accessRights: string;

    @ApiProperty()
    @Expose()
    initialTimestamp: Date;
}
