# Symbiota2 sample plugin

1. Clone this repository into the `plugins/` directory of the Symbiota2 source
2. Change the name and author of the package in [package.json](./package.json)
3. Remove `private: true` from [package.json](./package.json)
4. Use [NestJS](https://docs.nestjs.com/) to build the plugin
5. Add the name and version of the plugin to the `packages/api-server/package.json`
dependencies of the Symbiota2 source
6. Bootstrap the Symbiota2 source: `npm run bootstrap`
7. Import any NestJS modules from the plugin that you wish to use. Add them to 
the Symbiota2 core using `packages/api-server/src/plugin/plugin.config.ts` in
the Symbiota2 source
8. Build the Symbiota2 source: `npm run build`
9. Start Symbiota2, and any controllers/services made available by the plugin
should be available!
