# Symbiota2 API in Typescript
This project uses NestJS/TypeORM to connect to an existing Symbiota1 or 
Symbiota2 database and expose the Symbiota2 API.

At the start of the project, all TypeORM entities were configured to be 
compatible with a Symbiota database. However, it's recommended to back up
your existing database before running the following steps.


Each time you install/update Symbiota2, you should also start the server, then run database migrations:
```console
$ npm run start
[Nest] 14747   - 10/16/2020, 1:57:42 PM   [NestFactory] Starting Nest application...
[Nest] 14747   - 10/16/2020, 1:57:42 PM   [InstanceLoader] AppModule dependencies initialized +13ms
[Nest] 14747   - 10/16/2020, 1:57:42 PM   [InstanceLoader] PassportModule dependencies initialized +0ms
[Nest] 14747   - 10/16/2020, 1:57:42 PM   [InstanceLoader] PluginModule dependencies initialized +0ms
[Nest] 14747   - 10/16/2020, 1:57:42 PM   [InstanceLoader] ConfigHostModule dependencies initialized +1ms
[Nest] 14747   - 10/16/2020, 1:57:42 PM   [InstanceLoader] ConfigModule dependencies initialized +0ms
[Nest] 14747   - 10/16/2020, 1:57:42 PM   [InstanceLoader] AppConfigModule dependencies initialized +0ms
[Nest] 14747   - 10/16/2020, 1:57:42 PM   [DatabaseProvider] CLI config written successfully
...
```

In a separate terminal:
```
$ npm run typeorm:migration:run
query: SELECT * FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA` = 'symbscan' AND `TABLE_NAME` = 'migrations'
query: CREATE TABLE `symbscan`.`migrations` (`id` int NOT NULL AUTO_INCREMENT, `timestamp` bigint NOT NULL, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB
query: SELECT * FROM `symbscan`.`migrations` `migrations`  ORDER BY `id` DESC
No migrations are pending
```

The backend has the following core modules:
- [AppConfigModule](../config/src/app-config.module.ts): Responsible for
import Symbiota2 configuration from either environment variables or a `.env`
file
- [AuthModule](./src/auth/auth.module.ts): Responsible for authenticating users
via username and password and issuing a corresponding JWT and refresh token. 
This JWT includes roles that the user is assigned. Those roles are then 
validated against protected API routes.
- [ConfigurationModule](./src/configuration/configuration.module.ts): 
Responsible for retrieving key-value configuration for the web UI
- [Database](./src/database/database.module.ts): By far the largest module,
it contains all TypeORM definitions for Symbiota1 database tables, plus two
new Symbiota2 tables:
    - [RefreshToken](./src/database/entities/user/RefreshToken.entity.ts):
    Used to silently refresh/revoke a user's login JWT
    - [Configuration](./src/database/entities/Configuration.entity.ts):
    Used in ConfigurationModule (above)
- [Language](./src/language/language.module.ts): Used for retrieving different
language identifiers
- [Plugin](./src/plugin/plugin.module.ts): Used to load Symbiota2 plugins.
Each plugin is a NodeJS package with one or more NestJS modules. A default 
module is exported from the package and imported to Symbiota2 via the 
PluginModule at runtime. Plugins to be loaded are stored in 
`${APP_DATA_DIR}/plugins`. See the 
[sample plugin](https://gitlab.com/symbiota2/sample-plugin) for more.
- [SchemaVersion](./src/schema-version/schema-version.module.ts): Used to
retrieve the Symbiota schema updates that have been applied to the database.
- [User](./src/user/user.module.ts): Used for retrieving details on users of
the Symbiota2 instance

All modules are exported, and are available to plugins via the
[@symbiota2/backend](https://www.npmjs.com/package/@symbiota2/backend) 
NodeJS package.
