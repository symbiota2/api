import { OccurrenceModule } from '@symbiota2/api-plugin-occurrence';
import { CollectionModule } from '@symbiota2/api-plugin-collection';

export default [
    OccurrenceModule,
    CollectionModule
];
