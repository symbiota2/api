import {Module} from '@nestjs/common';
import { AppConfigModule } from '@symbiota2/api-config';
import {DatabaseProvider} from './database.provider';
import { entityProviders } from './providers';
import path from 'path';

const ENTITIES = path.join(__dirname, 'entities', '**', '*.entity.js');
const MIGRATIONS = path.join(__dirname, 'migrations', '*.js');

@Module({
    imports: [AppConfigModule],
    providers: [
        DatabaseProvider.register({
            entities: [ENTITIES],
            migrations: [MIGRATIONS]
        }),
        ...entityProviders
    ],
    exports: entityProviders
})
export class DatabaseModule { }
