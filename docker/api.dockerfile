FROM node:12

ENV NODE_ENV=production

ADD .. /app
WORKDIR app

RUN cd packages/api-server && npm i && npm run build && npm ln .
CMD s2-api
